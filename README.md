# solid-warp

This is a hacky example of accessing private data in a Solid pod from Rust.

Demo: https://www.youtube.com/watch?v=GyfSXSp7AlU

## Dependencies

Install [Bazel](https://bazel.build).

## Running

We use OAuth to access the pod. The examples will print a URL that you need
to open in your browser.

To read a private file from your pod:

```bash
bazel run :solid_warp -- \
  https://agentydragon.solidcommunity.net/private/ \
  read-graph
```

To write a small graph into your pod:

```bash
bazel run :solid_warp -- \
  https://agentydragon.solidcommunity.net/private/test.ttl \
  write-graph
```

NOTE: I have not tried this on any providers other than
https://solidcommunity.net. To try with a different issuer, pass something like
`--issuer=https://solidcommunity.net/`, including the trailing `/`.

NOTE: The server runs on port 3030, and this is currently hardcoded.

## Verbose logging

Pass `RUST_LOG=trace` in environment to get verbose logs.

## Lint

```bash
cargo clippy
```

## Cargo deps update

Aften changes to Cargo.toml, BUILD files need to be regenerated for the changed
dependencies:

```bash
cargo generate-lockfile && cargo raze
```

NOTE: `ring` crate will need to comment out a line; that's a hack to work around
apparent raze bug (?).

## TODO

* Use biscuit crate instead of jwt; it looks like it can handle JWKs on its own:
  https://docs.rs/biscuit/0.5.0/src/biscuit/jwk.rs.html#1394

## Licence

MIT
