// TODO(agentydragon): renewing tokens

extern crate base64;
extern crate biscuit;
extern crate chrono;
extern crate env_logger;
extern crate jwt;
extern crate log;
extern crate rand;
extern crate reqwest;
extern crate serde;
extern crate serde_json;
extern crate sophia;
extern crate structopt;
extern crate tokio;
extern crate url;
extern crate warp;

use biscuit::jwk::{
    AlgorithmParameters, EllipticCurve, EllipticCurveKeyParameters, EllipticCurveKeyType,
};
use chrono::{serde::ts_seconds, DateTime, Utc};
use jwt::{
    algorithm::{openssl::PKeyWithDigest, AlgorithmType},
    header::JoseHeader,
    SignWithKey, Token,
};
use log::*;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::TokenResponse;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge, RedirectUrl,
    Scope, TokenUrl,
};
use openssl::bn::{BigNum, BigNumContext};
use openssl::ec::{EcGroup, EcKey, EcPointRef};
use openssl::hash::MessageDigest;
use openssl::nid::Nid;
use openssl::pkey::PKey;
use openssl::pkey::Private;
use rand::thread_rng;
use rand::Rng;
use reqwest::Method;
use reqwest::{
    header::{HeaderMap, HeaderName, HeaderValue},
    StatusCode,
};
use serde::{Deserialize, Serialize};
use sophia::graph::inmem::FastGraph;
use sophia::graph::MutableGraph;
use sophia::ns::Namespace;
use sophia::parser::turtle::TurtleParser;
use sophia::parser::TripleParser;
use sophia::serializer::turtle::TurtleSerializer;
use sophia::serializer::Stringifier;
use sophia::serializer::TripleSerializer;
use sophia::triple::stream::TripleSource;
use std::error::Error;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::time::{SystemTime, UNIX_EPOCH};
use std::{
    fmt,
    fmt::{Display, Formatter},
};
use structopt::StructOpt;
use tokio::sync::mpsc;
use url::Url;
use warp::Filter;

#[derive(StructOpt, Debug)]
enum Command {
    ReadGraph,
    WriteGraph,
}

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(subcommand)]
    cmd: Command,
    url: Url,
    #[structopt(long, default_value = "https://solidcommunity.net/")]
    issuer: String,
}

const PORT: u16 = 3030;
// const REFRESH_TOKEN: &str = "refresh_token";
const CLIENT_SECRET_BASIC: &str = "client_secret_basic";

/// "DPoP" HTTP header
const HEADER_DPOP: &str = "DPoP";

fn get_redirect_uri() -> String {
    return format!("http://localhost:{}", PORT);
}

fn base64_urlsafe_nopad(data: &[u8]) -> String {
    base64::encode_config(data, base64::URL_SAFE_NO_PAD)
}

fn make_random_string<R>(rng: &mut R) -> String
where
    R: Rng,
{
    let mut random_data = [0u8; 64];
    rng.fill(&mut random_data);
    base64_urlsafe_nopad(&random_data)
}

#[derive(Serialize)]
struct DpopJwtHeader {
    typ: String,
    alg: AlgorithmType,
    jwk: AlgorithmParameters, // EllipticCurveKeyParameters,
}

fn from_es256_public_key(
    pubkey: &EcPointRef,
    group: &EcGroup,
) -> Result<AlgorithmParameters, openssl::error::ErrorStack> {
    let mut ctx = BigNumContext::new()?;
    let mut x = BigNum::new()?;
    let mut y = BigNum::new()?;
    pubkey.affine_coordinates_gfp(&group, &mut x, &mut y, &mut ctx)?;
    Ok(AlgorithmParameters::EllipticCurve(
        EllipticCurveKeyParameters {
            key_type: EllipticCurveKeyType::EC,
            curve: EllipticCurve::P256,
            x: x.to_vec(),
            y: y.to_vec(),
            d: None, // Private key
        },
    ))
}

impl JoseHeader for DpopJwtHeader {
    fn algorithm_type(&self) -> AlgorithmType {
        AlgorithmType::Es256
    }
}

#[derive(Serialize)]
struct DpopClaims<'a> {
    jti: String,
    #[serde(rename = "htm")]
    http_method: &'a str,
    #[serde(rename = "htu")]
    http_url: &'a str,
    #[serde(with = "ts_seconds", rename = "iat")]
    issued_at: DateTime<Utc>,
}

// needs serde
#[derive(Deserialize, Debug)]
struct OpenIDConfiguration {
    registration_endpoint: Url,
    authorization_endpoint: Url,
    token_endpoint: Url,

    issuer: String,
    jwks_uri: Url,
    response_types_supported: Vec<String>,
    token_types_supported: Vec<String>,
    response_modes_supported: Vec<String>,
    grant_types_supported: Vec<String>,
    subject_types_supported: Vec<String>,
    id_token_signing_alg_values_supported: Vec<String>,
    // TODO(agentydragon): should this be one-or-array?
    token_endpoint_auth_methods_supported: String,
    token_endpoint_auth_signing_alg_values_supported: Vec<String>,
    display_values_supported: Vec<String>,
    claim_types_supported: Vec<String>,
    //claims_suported: Vec<String>,
    claims_parameter_supported: bool,
    request_parameter_supported: bool,
    request_uri_parameter_supported: bool,
    require_request_uri_registration: bool,
    check_session_iframe: Option<Url>,
    end_session_endpoint: Option<Url>,
    userinfo_endpoint: Url,
}

#[derive(Debug, Clone)]
struct ClientSecretBasicNotAvailable;

impl Display for ClientSecretBasicNotAvailable {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "client_secret_basic not available")
    }
}
impl Error for ClientSecretBasicNotAvailable {}

async fn fetch_openid_configuration(issuer: &str) -> Result<OpenIDConfiguration, Box<dyn Error>> {
    let url = format!("{}.well-known/openid-configuration", issuer);
    let response = reqwest::get(&url).await?;
    check_http_status(&response, StatusCode::OK)?;
    let openid_configuration = response.json::<OpenIDConfiguration>().await?;
    if openid_configuration.token_endpoint_auth_methods_supported != CLIENT_SECRET_BASIC {
        return Err(Box::new(ClientSecretBasicNotAvailable {}));
    }
    Ok(openid_configuration)
}

#[derive(Serialize)]
struct ClientRegistrationRequest {
    redirect_uris: Vec<String>,
    client_name: String,
    // TODO(agentydragon): "client_name#ja-Jpan-JP": "\u30AF\u30E9\u30A4\u30A2\u30F3\u30C8\u540D"
    token_endpoint_auth_method: String,
    // TODO(agentydragon): Optional things we might add: client_uri, logo_uri,
    // contacts, tos_uri, policy_uri, jkws_uri, software_id, software_version
}

#[derive(Deserialize)]
struct ClientRegistrationResponse {
    redirect_uris: Vec<String>,
    client_id: String,
    client_secret: String,
    response_types: Vec<String>,
    grant_types: Vec<String>,
    application_type: String,
    client_name: String,
    id_token_signed_response_alg: String,
    token_endpoint_auth_method: String,
    registration_access_token: String,
    registration_client_uri: Url,
    #[serde(with = "ts_seconds")]
    client_id_issued_at: DateTime<Utc>,
    #[serde(with = "ts_seconds")]
    client_secret_expires_at: DateTime<Utc>,
}

/// Returns new client ID.
async fn register_client(
    client: &reqwest::Client,
    provider_config: &OpenIDConfiguration,
) -> Result<ClientRegistrationResponse, Box<dyn Error>> {
    let response = client
        .post(provider_config.registration_endpoint.clone())
        .json(&ClientRegistrationRequest {
            redirect_uris: vec![get_redirect_uri()],
            client_name: String::from("Rai's awesome thing"),
            token_endpoint_auth_method: String::from(CLIENT_SECRET_BASIC),
        })
        .send()
        .await?;
    check_http_status(&response, StatusCode::CREATED)?;
    let response = response.json::<ClientRegistrationResponse>().await?;
    Ok(response)
}

#[derive(Deserialize, Debug, Clone)]
struct OAuthCallbackRequest {
    code: AuthorizationCode,
    state: String,
}

#[derive(Debug, Clone)]
struct UnexpectedHttpStatus {
    expected: StatusCode,
    actual: StatusCode,
    headers: HeaderMap,
    url: Url,
}

impl Display for UnexpectedHttpStatus {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "unexpected http status on {} (expected {}, got {}); headers: {:?}",
            self.url, self.expected, self.actual, self.headers
        )
    }
}

impl Error for UnexpectedHttpStatus {}

fn check_http_status(
    response: &reqwest::Response,
    expected_status: StatusCode,
) -> Result<(), UnexpectedHttpStatus> {
    match response.status() {
        s if s == expected_status => Ok(()),
        s => Err(UnexpectedHttpStatus {
            expected: expected_status,
            actual: s,
            headers: response.headers().clone(),
            url: response.url().clone(),
        }),
    }
}

// #[derive(Serialize)]
// struct RefreshTokenRequest<'a> {
//     grant_type: &'a str,
//     refresh_token: &'a str,
//     //client_id: &'a str,
//     //client_secret: &'a str,
// }

#[derive(Deserialize, Debug)]
struct AccessTokenClaims {
    // https://agentydragon.solidcommunity.net/profile/card#me
    webid: String,

    /// Issuer. https://tools.ietf.org/html/rfc7519#section-4.1.1
    #[serde(rename = "iss")]
    issuer: String,

    /// Expiration time.
    /// https://tools.ietf.org/html/rfc7519#section-4.1.6
    #[serde(with = "ts_seconds", rename = "exp")]
    expiration_time: DateTime<Utc>,

    client_id: String,

    /// Issued at.
    /// https://tools.ietf.org/html/rfc7519#section-4.1.6
    #[serde(with = "ts_seconds", rename = "iat")]
    issued_at: DateTime<Utc>,

    /// Audience.
    /// https://tools.ietf.org/html/rfc7519#section-4.1.3
    // solid
    #[serde(rename = "aud")]
    audience: String,

    // cnf
    // "cnf": Object({"jkt": String("cQZwUTYejqpoaRZMV3hBoweOFS97edr2QUKl0SQTd30")})
    // https://curity.io/resources/architect/oauth/dpop-overview/
    // base64 encoding of sha-256 thumbprint of public key used to sign the dpop token
    /// Subject.
    /// https://tools.ietf.org/html/rfc7519#section-4.1.2
    #[serde(rename = "sub")]
    subject: String,
}

impl<R> AuthenticatedClient<R>
where
    R: Rng,
{
    // Pending https://github.com/solid/node-solid-server/issues/1533
    //async fn refresh_access_token(&mut self) -> Result<(), Box<dyn Error>> {
    //    //self.key = EcKey::generate(&self.curve)?;

    //    let url = &self.client.openid_configuration.token_endpoint;
    //    // https://stackoverflow.com/questions/55576538/401-malformed-http-basic-authorization-header-error-on-executing-api-through-re
    //    let auth_header = format!(
    //        "Basic {}",
    //        base64_urlsafe_nopad(
    //            // TODO: percent-encode?
    //            format!("{}:{}", self.client.client_id, self.client.client_secret).as_bytes()
    //        )
    //    );

    //    // // "malformed credentials":
    //    // let auth_header = format!(
    //    //     "Basic {}:{}",
    //    //     self.client.client_id, self.client.client_secret
    //    // );
    //    info!("auth header: {}", &auth_header);
    //    let response = self
    //        .http_client
    //        .post(url.clone())
    //        .form(&RefreshTokenRequest {
    //            grant_type: REFRESH_TOKEN,
    //            refresh_token: self.refresh_token.as_ref().unwrap(),
    //            //client_id: &self.client.client_id,
    //            //client_secret: &self.client.client_secret,
    //            //client_id: &self.client.client_id,
    //        })
    //        .headers(self.add_dpop_header(&url, &Method::POST, HeaderMap::new())?)
    //        // https://github.com/panva/node-openid-client/blob/588bee948b1af83446b7e3d8f4076131d1f2d343/lib/helpers/client.js#L86
    //        // TODO: client secret and id should also be URL encoded
    //        .header("Authorization", auth_header)
    //        .send()
    //        .await?;
    //    //check_http_status(&response, StatusCode::OK)?;
    //    //let response = response.json::<AccessTokenResponse>().await?;
    //    info!(
    //        "Response: {} {:?}",
    //        response.status(),
    //        response.text().await?
    //    );
    //    //let info: Token<AccessTokenHeader, AccessTokenClaims, _> =
    //    //    Token::parse_unverified(&response.access_token)?;
    //    //info!("access token: {:?} {:?}", info.header(), info.claims());
    //    //self.access_token = Some(response.access_token);
    //    //self.refresh_token = Some(response.refresh_token);

    //    Ok(())
    //}

    fn request(
        &mut self,
        method: Method,
        url: Url,
    ) -> Result<reqwest::RequestBuilder, Box<dyn Error>> {
        let dpop_header = self.signing_client.get_dpop_header(&url, &method)?;
        Ok(reqwest::Client::new()
            .request(method, url)
            .header(
                reqwest::header::AUTHORIZATION,
                HeaderValue::from_str(&format!("DPoP {}", self.access_token.secret()))?,
            )
            .header(dpop_header.0, dpop_header.1))
    }

    async fn get_graph(&mut self, url: Url) -> Result<FastGraph, Box<dyn Error>> {
        let parser = TurtleParser {
            base: Some(String::from(url.as_str())),
        };
        let response = self.request(Method::GET, url)?.send().await?;
        check_http_status(&response, StatusCode::OK)?;
        let string = response.text().await?;

        let graph: FastGraph = parser.parse_str(&string).collect_triples()?;
        Ok(graph)
    }

    async fn put_graph(&mut self, url: Url, graph: &FastGraph) -> Result<(), Box<dyn Error>> {
        let mut nt_stringifier = TurtleSerializer::new_stringifier();
        nt_stringifier.serialize_graph(graph)?;
        let s = nt_stringifier.to_string();

        let request = self
            .request(Method::PUT, url)?
            .body(s)
            .header(reqwest::header::CONTENT_TYPE, "text/turtle");
        let response = request.send().await?;
        // check_http_status(&response, StatusCode::CREATED)?;
        Ok(())
    }
}

async fn obtain_code_and_state() -> Result<OAuthCallbackRequest, Box<dyn Error>> {
    let request: Arc<Mutex<Option<OAuthCallbackRequest>>> = Arc::new(Mutex::new(None));
    let (tx, mut rx) = mpsc::channel(1);

    let c_request = Arc::clone(&request);

    let routes = warp::query().map(move |oauth_request: OAuthCallbackRequest| {
        tx.try_send(()).expect("shutdown webserver");
        *c_request.lock().unwrap() = Some(oauth_request);
        "We got an access token, you can go back to your CLI.".to_string()
    });

    let (_addr, server) =
        warp::serve(routes).bind_with_graceful_shutdown(([127, 0, 0, 1], PORT), async move {
            rx.recv().await;
        });

    tokio::task::spawn(server).await?;

    Ok(Arc::try_unwrap(request)
        .expect("Arc has >1 strong reference?")
        .into_inner()
        .expect("cannot lock mutex?")
        .expect("no OAuthCallbackRequest present"))
}

/// HTTP client for oauth2 that signs requests with a DPoP header.
/// Used for requests that do not yet have an access token, like the first
/// token request.
struct DPoPSigningHttpClient<R>
where
    R: Rng,
{
    curve: EcGroup,
    key: EcKey<Private>,
    /// For generating random nonces for DPoP header.
    rng: R,
}

impl<R> DPoPSigningHttpClient<R>
where
    R: Rng,
{
    fn new(rng: R) -> Result<Self, Box<dyn Error>> {
        let group = EcGroup::from_curve_name(Nid::X9_62_PRIME256V1)?;
        let key = EcKey::generate(&group)?;
        Ok(DPoPSigningHttpClient {
            curve: group,
            key,
            rng,
        })
    }

    fn get_keypair(&self) -> Result<PKeyWithDigest<Private>, Box<dyn Error>> {
        Ok(PKeyWithDigest::<Private> {
            digest: MessageDigest::sha256(),
            key: PKey::from_ec_key(self.key.clone())?,
        })
    }

    fn make_token_for(&mut self, url: &Url, method: &Method) -> Result<String, Box<dyn Error>> {
        let header = DpopJwtHeader {
            typ: String::from("dpop+jwt"),
            alg: AlgorithmType::Es256,
            jwk: from_es256_public_key(self.key.public_key(), &self.curve)?,
        };

        let claims = DpopClaims {
            jti: make_random_string(&mut self.rng),
            http_method: method.as_str(),
            http_url: url.as_str(),
            issued_at: Utc::now(),
        };

        let token = Token::new(header, claims).sign_with_key(&self.get_keypair()?)?;
        Ok(String::from(token.as_str()))
    }

    /// Builds a DPoP header for the given URL and method.
    fn get_dpop_header(
        &mut self,
        url: &Url,
        method: &Method,
    ) -> Result<(HeaderName, HeaderValue), Box<dyn Error>> {
        Ok((
            HeaderName::from_str(HEADER_DPOP).unwrap(),
            HeaderValue::from_str(&self.make_token_for(url, method)?)?,
        ))
    }

    /// Signs the request before calling `oauth2::reqwest::http_client`.
    async fn execute_async(
        &mut self,
        mut request: oauth2::HttpRequest,
    ) -> Result<oauth2::HttpResponse, impl std::error::Error> {
        // TODO
        let dpop_header = self.get_dpop_header(&request.url, &request.method).unwrap();
        request.headers.insert(dpop_header.0, dpop_header.1);
        async_http_client(request).await
    }
}

struct AuthenticatedClient<R>
where
    R: Rng,
{
    access_token: oauth2::AccessToken,
    refresh_token: Option<String>,
    signing_client: DPoPSigningHttpClient<R>,
}

async fn work(opt: Opt) -> Result<(), Box<dyn Error>> {
    let client = reqwest::Client::new();
    let openid_configuration = fetch_openid_configuration(&opt.issuer).await?;
    let client_registration = register_client(&client, &openid_configuration).await?;

    // Create an OAuth2 client by specifying client stuff.
    let oauth2_client = BasicClient::new(
        ClientId::new(client_registration.client_id),
        Some(ClientSecret::new(client_registration.client_secret)),
        AuthUrl::new(openid_configuration.authorization_endpoint.to_string())?,
        Some(TokenUrl::new(
            openid_configuration.token_endpoint.to_string(),
        )?),
    )
    .set_redirect_uri(RedirectUrl::new(get_redirect_uri())?);

    // Generate a PKCE challenge.
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    let (the_url, csrf_token) = oauth2_client
        .authorize_url(CsrfToken::new_random)
        .add_scope(Scope::new("openid".to_string()))
        // TODO: https://solid.github.io/authentication-panel/solid-oidc-primer/ says "open_id"
        // TODO: https://solid.github.io/authentication-panel/solid-oidc-primer/ says "profile"
        // offline_access also requests a refresh token.
        .add_scope(Scope::new("offline_access".to_string()))
        .set_pkce_challenge(pkce_challenge)
        .url();

    println!(
        "Open this URL in your browser to authorize access to the pod: {}",
        the_url
    );

    let oauth_callback_request = obtain_code_and_state().await?;
    assert_eq!(&oauth_callback_request.state, csrf_token.secret());

    let mut signing_client = DPoPSigningHttpClient::new(thread_rng())?;

    // Exchange auth code for access token.
    let token_result = oauth2_client
        .exchange_code(oauth_callback_request.code.clone())
        .set_pkce_verifier(pkce_verifier)
        .request_async(|request| signing_client.execute_async(request))
        .await?;

    let mut auth_client = AuthenticatedClient {
        signing_client,
        access_token: token_result.access_token().clone(),
        refresh_token: token_result.refresh_token().map(|rt| rt.secret().clone()),
    };

    match opt.cmd {
        Command::ReadGraph => {
            let graph = auth_client.get_graph(opt.url.clone()).await?;
            let mut nt_stringifier = TurtleSerializer::new_stringifier();
            println!(
                "Graph read from {}:\n{}",
                &opt.url,
                nt_stringifier.serialize_graph(&graph)?.as_str()
            );
        }
        Command::WriteGraph => {
            let mut new_graph = FastGraph::new();
            let ex = Namespace::new("http://example.org/")?;
            let foaf = Namespace::new("http://xmlns.com/foaf/0.1/")?;
            new_graph.insert(&ex.get("bob")?, &foaf.get("knows")?, &ex.get("alice")?)?;
            auth_client.put_graph(opt.url.clone(), &new_graph).await?;
            println!("Example graph was written to {}.", &opt.url);
        }
    }

    // Pending https://github.com/solid/node-solid-server/issues/1533
    // auth_client.refresh_access_token().await?;

    // TODO(agentydragon): revoke the token

    Ok(())
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let opt = Opt::from_args();
    info!("{:?}", opt);

    work(opt).await.unwrap();
}
